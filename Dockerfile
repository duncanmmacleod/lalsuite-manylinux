ARG base=quay.io/pypa/manylinux1_x86_64
FROM $base

# Install dependencies available through yum
RUN yum install -y -q zlib-devel fftw3-devel libxml2-devel mpich2-devel openssl-devel pcre-devel xz && yum clean all

# Configure, make, and make install these packages from source
COPY build.bash /
RUN /build.bash 5eaa2e06d8e4197fd02194051db1e518325dbb074a4c55a91099ad9c55193874f577764afc9029409a41bd520a95154095f26e33ef5add5c102bb2c1d98d33eb http://downloads.sourceforge.net/project/swig/swig/swig-3.0.12/swig-3.0.12.tar.gz
RUN /build.bash f828ee9d63533effe1ad358230e5ce7b64c5016e49291d9533575f713cbfba496290fc0151fd9617898bdf36785984ddb38a9207f529d7702d4e23838fe050d8 http://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.10/hdf5-1.10.5/src/hdf5-1.10.5.tar.gz
RUN /build.bash 0d1a8d36fb7c0ff6a1be9cbbe6a1995483db60503fdee651654a264e8e145230140c13f63d5c5d805211c2962f44a4defac96938a3d4e5268a6ce58e7d909af8 http://lappweb.in2p3.fr/virgo/FrameL/libframe-8.30.tar.gz
RUN /build.bash f7c5d7f8862e088c0b7b7050f95af5e8c65234988f8cd337c32a2c2ad7b40030b881b0ae768a66c7f8e0646e1b6a2f64714a66bee6527514af6be60f824b038b http://software.ligo.org/lscsoft/source/metaio-8.5.1.tar.gz
RUN /build.bash f553f9bbedc3ffabf939bb507d3c41d6dc0d889b3a0835c23a7aff90ba98d7f9a6217527d69bc8984ce9bd9664681baeec5b1555d2ab51aaca003fdb5a08127e http://heasarc.gsfc.nasa.gov/FTP/software/fitsio/c/cfitsio3450.tar.gz all shared
RUN /build.bash 20eec83a3e71a578e9f7615e631ec712774cde3e4e41a4117e26a148dbee611de5133c751ff0c5c9c6b47f66fedd8de4b3cd23146c1006edc38e32913eae7cd1 http://downloads.sourceforge.net/project/healpix/Healpix_3.30/chealpix-3.30.0.tar.gz
RUN /build.bash 5b4c5c023f9029ce220f4e09aa4d0234fed94d42ac224d58fda095fe0532d54237a7c33278f8b5d0ba051f6004486edb38d0adb4fcb49337a8c1d8a18cf4a24a http://ftpmirror.gnu.org/gsl/gsl-2.5.tar.gz
RUN rm -f /build.bash

# Install pip dependencies
RUN /opt/python/cp27-cp27m/bin/pip  install -q --no-cache-dir glob2 virtualenv numpy==1.7.0
RUN /opt/python/cp27-cp27mu/bin/pip install -q --no-cache-dir glob2 virtualenv numpy==1.7.0
RUN /opt/python/cp34-cp34m/bin/pip  install -q --no-cache-dir glob2 virtualenv numpy==1.7.0
RUN /opt/python/cp35-cp35m/bin/pip  install -q --no-cache-dir glob2 virtualenv numpy==1.7.0
RUN /opt/python/cp36-cp36m/bin/pip  install -q --no-cache-dir glob2 virtualenv numpy==1.7.0
RUN /opt/python/cp37-cp37m/bin/pip  install -q --no-cache-dir glob2 virtualenv numpy==1.14.5

# FIXME: workaround for https://github.com/pypa/auditwheel/issues/89
RUN /opt/python/cp36-cp36m/bin/pip install -q --no-cache-dir git+https://github.com/lpsinger/auditwheel@fix-duplicate-libs
