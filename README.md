This repository creates a Docker image based on
[manylinux](https://github.com/pypa/manylinux) that contains the build
dependencies for LALSuite.
