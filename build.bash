#!/bin/bash

# Echo commands, and halt on error
set -ex

# Unpack command line arguments
hash=$1
url=$2
shift 2
makeargs="$*"

# Download and verify source
curl -Ls -o src.tar.gz $url
echo "$hash  src.tar.gz" | sha512sum --check

# Build and install
mkdir build
pushd build
tar --strip-components 1 -xzf ../src.tar.gz
./configure --prefix=/usr --libdir=/usr/lib64
make -j $makeargs
make install
popd

# Cleanup
rm -rf build src.tar.gz
